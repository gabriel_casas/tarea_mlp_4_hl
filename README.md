
# tarea_mlp_4_hl
 
## Tarea de multi layer perceptron con 4 hidden layers
 
El proyecto esta en el archivo [MLP_4_HL.py](./MLP_4_HL.py)
 
Se utilizan 200 imágenes del dataset cifar [data_batch_1](./cifar/data_batch_1)
y 200 imágenes del dataset cifar [data_batch_1](./cifar/data_batch_1)
 
está compuesta por 4 capas de **880, 690, 200, 2**
 
los pesos w tienen las siguientes dimensiones:
```
W1 = (3072, 880)
W2 = (880, 690)
W3 = (690, 200)
W4 = (200, 2)
```
Para las 3 primeras capas se usa la funcion de activacion **Relu** y para la última capa se utiliza **Signoid**
 
Observación: Se notó que los errores se reducen, pero el valor más pequeño que se alcanza es igual a la dimensión de **y**, si y tiene dimensiones (200,2) el valor mínimo de la función de pérdida es **200**
 
Gráficas de las pruebas con distintos **lr** y número de **neuronas**
 
![MLP-README.md](./img/2019-12-18_11-44-30.png)
![MLP-README.md](./img/2019-12-18_11-47-19.png)
![MLP-README.md](./img/2019-12-19_15-30-45.png)
![MLP-README.md](./img/2019-12-19_20-48-50.png)