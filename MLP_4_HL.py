#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 17:52:29 2019

@author: gabriel casas
@comment: 
@repository: https://gitlab.com/gabriel_casas/tarea_mlp_4_hl
@colab: https://colab.research.google.com/drive/15hEYOX2AGrweO851wWuYmvzN4Ka2gb6l
"""

import numpy as np
from numpy.random import randn
import matplotlib.pyplot as plt
import pickle

class mlp:
    def __init__(self, x=np.array([]), y=np.array([]), lr=0.0001, epoch=1000):
        self.x = x
        self.y = y
        self.ch = {}
        self.epoch = epoch
        # defining w for 4 layers
        self.w = []
        # defining losses
        self.losses = np.zeros([epoch, 1])
        # gradient values for 4 layers
        self.v_w = [0, 0, 0, 0]
        # learning rate
        self.lr = lr

    def unpickle(self, file):
        with open(file, 'rb') as fo:
            dict = pickle.load(fo, encoding = 'bytes')
            return dict

    def initWeigth(self, i_units=1, h_units=[2,2], o_units=2):
        # also init y_pred
        self.y_pred = np.zeros((1, self.y.shape[1]))
        self.w = {}
        std = 1/np.sqrt(i_units/2)
        w_i = std*randn(i_units,h_units[0])
        #self.w.append(np.asarray(w_i))
        self.w['w1'] = np.asarray(w_i)
        
        for i in range(len(h_units) -1):
            std = 1/np.sqrt(h_units[i]/2)
            w_i = std*randn(h_units[i],h_units[i+1])
            #self.w.append(np.asarray(w_i))
            self.w['w'+str(i + 2)] = np.asarray(w_i)
            
        std = 1/np.sqrt(h_units[-1]/2)
        w_i = std*randn(h_units[-1],o_units)
        #self.w.append(np.asarray(w_i))
        self.w['w'+str(len(h_units) + 1)] = np.asarray(w_i)
        return self.w

    def relu(self, z):
        return np.maximum(0, z)

    def sigmoid(self, Z):
      return 1/(1+np.exp(-Z))

    def dRelu(self, x):
        x[x<=0] = 0
        x[x>0] = 1
        return x
    
    def dSigmoid(self, Z):
      s = 1/(1+np.exp(-Z))
      dZ = s * (1-s)
      return dZ

    def loss(self, y, y_t):
        return (np.square(y_t - y).sum())

    def forward(self):
        
        #Z1 = self.w['w1'].dot(self.x) # + self.param['b1'] 
        Z1 = self.x.dot(self.w['w1'])
        A1 = self.relu(Z1)
        self.ch['z1'],self.ch['a1'] = Z1, A1
        
        #Z2 = self.w['w2'].dot(A1)
        Z2 = A1.dot(self.w['w2'])
        A2 = self.relu(Z2)
        self.ch['z2'],self.ch['a2'] = Z2, A2
        
        #Z3 = self.w['w3'].dot(A2)
        Z3 = A2.dot(self.w['w3'])
        A3 = self.relu(Z3)
        self.ch['z3'],self.ch['a3'] = Z3, A3
        
        #Z4 = self.w['w5'].dot(A3)
        Z4 = A3.dot(self.w['w4'])
        A4 = self.sigmoid(Z4)
        self.ch['z4'],self.ch['a4'] = Z4, A4
        
        self.y_pred = A4
        
        loss=self.loss(self.y, self.y_pred)
        
        return self.y_pred, loss

    def backward(self):
        
        # dLoss_y_pred = - (np.divide(self.y, self.y_pred ) - np.divide(1 - self.y, 1 - self.y_pred))
        grad_y = 2.0 * (self.y_pred - self.y)
        dLoss_A4 = self.ch['a4'].T.dot(grad_y)
        dLoss_W4 = self.dSigmoid(dLoss_A4.T.dot(self.w['w4'].T))
        self.dLoss_W4 = dLoss_W4
        self.dLoss_A4 = dLoss_A4
        
        dLoss_Z3 = self.ch['z3']
        dLoss_A3 = self.ch['a3'].T.dot(dLoss_Z3)
        dLoss_W3 = self.dRelu(dLoss_A3.dot(self.w['w3'].T))
        self.dLoss_W3 = dLoss_W3
        self.dLoss_A3 = dLoss_A3
        
        dLoss_Z2 = self.ch['z2']
        dLoss_A2 = self.ch['a2'].T.dot(dLoss_Z2)
        dLoss_W2 = self.dRelu(dLoss_A2.dot(self.w['w2'].T))
        self.dLoss_W2 = dLoss_W2
        self.dLoss_A2 = dLoss_A2
        
        dLoss_Z1 = self.ch['z1']
        dLoss_A1 = self.ch['a1'].T.dot(dLoss_Z1)
        dLoss_W1 = self.dRelu(dLoss_A1.dot(self.w['w1'].T))
        self.dLoss_W1 = dLoss_W1
        self.dLoss_A1 = dLoss_A1
        
        self.w["w1"] = self.w["w1"] - self.lr * dLoss_W1.T
        self.w["w2"] = self.w["w2"] - self.lr * dLoss_W2.T
        self.w["w3"] = self.w["w3"] - self.lr * dLoss_W3.T
        self.w["w4"] = self.w["w4"] - self.lr * dLoss_W4.T


mlp4l =  mlp()
data1 = mlp4l.unpickle('./cifar/data_batch_1')
data2 = mlp4l.unpickle('./cifar/data_batch_2')

images1 = data1[b'data']
labels1 = data1[b'labels']
images2 = data2[b'data']
labels2 = data2[b'labels']

matches0_1 = [i for i,x in enumerate(labels1) if x==0]
matches9_1 = [i for i,x in enumerate(labels1) if x==9]
matches0_2 = [i for i,x in enumerate(labels2) if x==0]
matches9_2 = [i for i,x in enumerate(labels2) if x==9]

matches0_1 = matches0_1[0:100]
matches9_1 = matches9_1[0:100]
matches0_2 = matches0_2[0:100]
matches9_2 = matches9_2[0:100]

d0_1 = [images1[i] for i in tuple(matches0_1)]
d9_1 = [images1[i] for i in tuple(matches9_1)]
d0_2 = [images2[i] for i in tuple(matches0_2)]
d9_2 = [images2[i] for i in tuple(matches9_2)]

y0_1 = np.repeat(np.array([1,0]), 100, axis=0)
y9_1 = np.repeat(np.array([0,1]), 100, axis=0)
y0_2 = np.repeat(np.array([1,0]), 100, axis=0)
y9_2 = np.repeat(np.array([0,1]), 100, axis=0)

x=np.array(d0_1+d9_1+d0_2+d9_2)
x=x-np.mean(x)
yp1 = np.vstack(([y0_1], [y9_1]));
yp2 = np.vstack(([y0_2], [y9_2]));

y=np.concatenate((yp1,yp2), axis=1).T

# defining mlp class
mlp =  mlp(x, y, lr=0.0001, epoch=300)

# defining w for 4 layers at h_units=[1000, 500, 100] 
w = mlp.initWeigth(i_units=3072, h_units=[880, 690, 200], o_units=2)
'''
print(w['w1'].shape)
print(w['w2'].shape)
print(w['w3'].shape)
print(w['w4'].shape)
'''
losses = np.zeros([mlp.epoch, 1])
for t in range(mlp.epoch):
    y_pred, loss = mlp.forward()
    losses[t] = loss
    print('{0} ,  loss:: {1}'.format(t, loss))
    mlp.backward()

y_pred = mlp.y_pred
plt.figure(1)
plt.plot(range(mlp.epoch),losses,'r*')
plt.show()
